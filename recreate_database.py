from app import app
from app.models import *

"""
Run this script to recreate database
"""

app.config.from_object('config')
db.drop_all()
db.create_all()
