# Description #
Demo online books library application using the Flask framework, SQLAlchemy, WTForms, Flask-Login and Bcrypt.

# Installation #
```
#!

git clone https://dgtv@bitbucket.org/dgtv/olibrary.git
cd olibrary
pip install -r requirements.txt
python recreate_database.py
```

# Usage #

```
#!

python run.py
```