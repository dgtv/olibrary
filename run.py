from app import app

import os

app.config.from_object('config')
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])
app.run(host='0.0.0.0', port=os.environ.get('PORT', 5000))
