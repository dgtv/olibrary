from datetime import datetime
from app import db, bcrypt

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Unicode(256), nullable=False)
    password = db.Column(db.Unicode(256), nullable=False)
    role = db.Column(db.Unicode(256), default=u'user')
    active = db.Column(db.Boolean, default=True)

    def __init__(self, email, password, role=u'user', active=True):
        self.email = email
        self.set_password(password)
        self.role = role
        self.active = active

    def __repr__(self):
        return '<User(%d:%s)>' % (self.id, self.email)

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        if bcrypt.check_password_hash(self.password, password):
            return True
        else:
            return False

    # Flask-Login
    def is_authenticated(self):
        return True

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id


class Book(db.Model):
    __tablename__ = 'books'

    id = db.Column(db.Integer, primary_key=True)
    isbn = db.Column(db.Unicode(80))
    authors = db.Column(db.Unicode(80))
    name = db.Column(db.Unicode(80))
    category = db.Column(db.Unicode(45))
    description = db.Column(db.Unicode(255))
    img_filename = db.Column(db.Unicode(80))
    insertion_date = db.Column(db.DateTime)
    pdf_filename = db.Column(db.Unicode(80))

    uploader_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    uploader = db.relationship('User',
                               backref=db.backref('books', lazy='dynamic'))

    def __init__(self, name, authors, category, uploader,
                 description=None, image_filename=None,
                 insertion_data=None, pdf_filename=None, isbn=None):
        self.name = name
        self.authors = authors
        self.category = category
        self.description = description
        self.isbn = isbn
        if insertion_data is None:
            insertion_data = datetime.utcnow()
        self.insertion_date = insertion_data
        self.uploader = uploader
        self.pdf_filename = pdf_filename
        self.img_filename = image_filename

    def __repr__(self):
        return '<Book "%s" - %s>' % (self.name, self.authors)


class BookAccess(db.Model):
    __tablename__ = 'bookaccesses'

    id = db.Column(db.Integer, primary_key=True)
    access_time = db.Column(db.DateTime)

    book_id = db.Column(db.Integer, db.ForeignKey('books.id'))
    book = db.relationship('Book',
                           backref=db.backref('accesses', lazy='dynamic'))

    def __init__(self, book, access_time=None):
        if access_time is None:
            access_time = datetime.utcnow()
        self.access_time = access_time
        self.book = book

    def __repr__(self):
        return '<Access: %s - %s>' % (self.book.name, str(self.access_time))


class DownloadInfo(db.Model):
    __tablename__ = 'downloadsinfo'

    id = db.Column(db.Integer, primary_key=True)
    download_time = db.Column(db.DateTime)

    book_id = db.Column(db.Integer, db.ForeignKey('books.id'))
    book = db.relationship('Book',
                           backref=db.backref('downloads', lazy='dynamic'))

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User',
                           backref=db.backref('downloads', lazy='dynamic'))

    def __init__(self, book, user, download_time=None):
        self.book = book
        self.user = user
        if download_time is None:
            download_time = datetime.utcnow()
        self.download_time = download_time

    def __repr__(self):
        return '<Download: %s - %s : %s' % (self.book.name, self.user.email,
                                            str(self.download_time))
