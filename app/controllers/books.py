from app import app, db, only_role
from app.models import Book, BookAccess, DownloadInfo
from app.forms import AddBook, EditBook
from flask import render_template, redirect, url_for, flash, request, abort, send_from_directory
from flask_login import current_user, login_required
from werkzeug.utils import secure_filename
from config import _basedir, USER_ROLES
import os
import random

"""
Functions for working with books
"""

##################
#    Utilites
##################

@app.route('/book-covers/<filename>')
def send_cover(filename):
    """
    Send book cover by image filename
    :param filename: cover filename in the server
    :return: book cover
    """
    uploads = os.path.join(_basedir, app.config['UPLOAD_FOLDER'])
    return send_from_directory(directory=uploads, filename=filename)


#########################
#    Visitor features
#########################

@app.route('/books')
@app.route('/books/<category>')
def books(category=None):
    if category is None:
        books = Book.query.all()
    else:
        books = Book.query.filter_by(category=category).all()
    return render_template('books.html', user=current_user,
                           books=books, categories=app.config['BOOK_CATEGORIES'])


@app.route('/book-details/<int:book_id>')
def book_details(book_id):
    book = Book.query.get_or_404(book_id)  # if the book is not exist redirect to 404 page

    # get suggested book by category
    suggestedBooks = Book.query.filter(
        Book.category == book.category).filter(
        Book.id.isnot(book.id)).all()
    if len(suggestedBooks) > 0:  # if exist choose one randomly
        suggestedBook = random.choice(suggestedBooks)
    else:
        suggestedBook = None

    # keeping statistics only for not uploader
    if current_user.get_id() != book.uploader_id:
        access = BookAccess(book)
        db.session.add(access)
        db.session.commit()

    return render_template('book-details.html', user=current_user,
                           book=book, suggestedBook=suggestedBook)


#############################
#    Simple User features
#############################

@app.route('/download/<int:book_id>')
@login_required
def download(book_id):
    # data preparation
    book = Book.query.get_or_404(book_id)  # if the book is not exist redirect to 404 page
    filename = book.pdf_filename
    uploads = os.path.join(_basedir, app.config['UPLOAD_FOLDER'])

    # keeping statistics only for not uploader
    if current_user.get_id() != book.uploader_id:
        download_info = DownloadInfo(book, current_user)
        db.session.add(download_info)
        db.session.commit()

    return send_from_directory(directory=uploads, filename=filename)


################################
#    Publisher User features
################################

@app.route('/overview')
@login_required
@only_role(USER_ROLES[1])
def overview():
    books = Book.query.filter_by(uploader_id=current_user.get_id()).all()
    access = []
    downloads = []
    for book in books:
        access.append(len(BookAccess.query.filter_by(
            book_id=book.id).all()))
        downloads.append(len(DownloadInfo.query.filter_by(
            book_id=book.id).all()))

    return render_template('overview.html', user=current_user,
                           books=books, access=access, downloads=downloads)


@app.route('/add-book', methods=['GET', 'POST'])
@login_required
@only_role(USER_ROLES[1])
def add_book():
    form = AddBook()
    if form.validate_on_submit():
        # save files on the server
        pdf_name = secure_filename(form.pdf.data.filename)
        img_name = secure_filename(form.img.data.filename)
        form.pdf.data.save(app.config['UPLOAD_FOLDER'] + pdf_name)
        form.img.data.save(app.config['UPLOAD_FOLDER'] + img_name)

        # create new Book and save it in database
        book = Book(name=form.name.data,
                    authors=form.authors.data,
                    category=form.category.data,
                    uploader=current_user,
                    description=form.description.data,
                    image_filename=img_name,
                    pdf_filename=pdf_name,
                    isbn=form.isbn.data)
        db.session.add(book)
        db.session.commit()
        flash('Book "%s" added succesfully' % book.name)

        return redirect(url_for('added_books'))

    return render_template('add-book.html', user=current_user,
                           form=form)


@app.route('/added-books')
@login_required
@only_role(USER_ROLES[1])
def added_books():
    books = Book.query.filter_by(uploader_id=current_user.get_id()).all()
    return render_template('added-books.html', user=current_user,
                           books=books)


@app.route('/view-book/<int:book_id>')
@login_required
@only_role(USER_ROLES[1])
def view_book(book_id):
    book = Book.query.get_or_404(book_id)  # if the book is not exist redirect to 404 page
    access = len(BookAccess.query.filter_by(
        book_id=book_id).all())
    downloads = len(DownloadInfo.query.filter_by(
        book_id=book_id).all())
    return render_template('view-book.html', user=current_user,
                           book=book, access=access, downloads=downloads)


@app.route('/edit-book/<int:book_id>', methods=['GET', 'POST'])
@login_required
@only_role(USER_ROLES[1])
def edit_book(book_id):
    form = EditBook()
    book = Book.query.get_or_404(book_id)
    book_name = book.name  # for flash message
    if form.validate_on_submit():
        book.name = form.name.data
        book.authors = form.authors.data
        book.category = form.category.data
        book.description = form.description.data
        book.isbn = form.isbn.data
        db.session.commit()
        flash('Book "%s" edited successfully' % book_name)

        return redirect(url_for('added_books'))

    return render_template('edit-book.html', user=current_user, form=form, book=book)


@app.route('/delete-book/<int:book_id>', methods=['GET', 'POST'])
@login_required
@only_role(USER_ROLES[1])
def delete_book(book_id):
    if request.method != 'POST':
        abort(404)
    else:
        book = Book.query.get_or_404(book_id)
        book_name = book.name  # for flash message
        if book.uploader_id == current_user.get_id():
            db.session.delete(book)
            db.session.commit()
            flash('Book "%s" deleted' % book_name)

    return redirect(url_for('added_books'))
