from app import app
from flask import render_template
from flask_login import current_user

"""
Custom error handlers
"""

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', user=current_user), 404


@app.errorhandler(403)
def no_access(e):
    return render_template('403.html', user=current_user), 403
