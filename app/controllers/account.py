from app import app, db
from app.forms import EditAccount
from flask import redirect, render_template, url_for, flash
from flask_login import current_user, login_required

@app.route('/edit-account', methods=['GET', 'POST'])
@login_required
def edit_account():
    form = EditAccount()
    if form.validate_on_submit():
        current_user.set_password(form.new_password.data)
        db.session.commit()
        flash('Password changed successfully')
        redirect(url_for('edit_account'))
    return render_template('edit-account.html', user=current_user, form=form)
