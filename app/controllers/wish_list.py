from app import app, db
from app.models import Book, User, BookAccess
from flask import render_template, request, redirect, url_for, session
from flask_login import current_user, login_required

"""
Functions for working with user books wishlist
"""

@app.route('/wishlist')
@login_required
def wishlist():
    if 'wish_books_id' not in session:  # creating before use
        session['wish_books_id'] = []

    wish_books = []
    for book_id in session['wish_books_id']:
        book = Book.query.get(book_id)
        wish_books.append(Book.query.get(book_id))

    return render_template('wishlist.html', wish_books=wish_books,
                           user=current_user)


@app.route('/add-to-wishlist/<int:book_id>')
@login_required
def add_to_wishlist(book_id):
    if 'wish_books_id' not in session:  # creating before use
        session['wish_books_id'] = []

    if book_id not in session['wish_books_id']:  # if the book not added before
        book = Book.query.get(book_id)
        if book is not None:  # if the book is present in database
            session['wish_books_id'].append(book.id)

    return redirect(url_for('wishlist'))


@app.route('/delete-wishlist-book/<int:book_id>')
@login_required
def delete_from_wishlist(book_id):
    if ('wish_books_id' in session and
                book_id in session['wish_books_id']):
        session['wish_books_id'].remove(book_id)
    return redirect(url_for('wishlist'))
