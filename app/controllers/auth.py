from app import app, bcrypt, db, login_manager
from app.forms import LoginForm, SignupForm
from app.models import User
from flask import redirect, render_template, request, url_for, session, abort
from flask_login import current_user, login_required, login_user, logout_user
from functools import wraps

"""
Functions for working with user authentication
"""


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(id=int(user_id)).first()


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('login'))


def only_role(role):
    """
    Decorator for access checking
    :param role: only user with role transmitted in the argument
                 has access to the decorated function
    :return: requested function if has access or abort with 403 code otherwise
    """
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if current_user.role != role:
                abort(403)  # may change to smth else
            else:
                return f(*args, **kwargs)
        return wrapper
    return decorator


@app.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm()
    if login_form.validate_on_submit():
        login_user(login_form.user, remember=login_form.remember)
        return redirect(request.args.get('next') or url_for('index'))
    return render_template('login.html', login_form=login_form, user=current_user)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    if 'wish_books_id' in session:  # clean wish books after logout
        session.pop('wish_books_id', None)
    return redirect(url_for('index'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    logout_user()
    signup_form = SignupForm()
    if signup_form.validate_on_submit():
        user = User(signup_form.email.data,
                    signup_form.password.data,
                    role=signup_form.role.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('index'))
    return render_template('signup.html', signup_form=signup_form, user=current_user)
