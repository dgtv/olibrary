from app import app
from flask import render_template
from flask_login import current_user


@app.route('/help')
def help_page():
    return render_template('help.html', user=current_user)
