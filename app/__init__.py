from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
bcrypt = Bcrypt(app)

# import controller there before use it
from controllers.index import *
from controllers.auth import *
from controllers.help import *
from controllers.books import *
from controllers.account import *
from controllers.wish_list import *
from controllers.errors import *