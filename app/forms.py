from app.models import User
from flask_wtf import Form
from wtforms.fields import BooleanField, PasswordField, StringField, RadioField, SelectField
from wtforms.validators import DataRequired, Email, EqualTo, Required, length
from flask_wtf.file import FileField, FileRequired, FileAllowed
from config import BOOK_CATEGORIES, USER_ROLES
from flask.ext.login import current_user

"""
WTForms classes
"""


class LoginForm(Form):
    email = StringField(u'Email', validators=[DataRequired(), Email()])
    password = PasswordField(u'Password', validators=[DataRequired()])
    remember = BooleanField(u'Remember Me', default=False)

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate_on_submit(self):
        rv = Form.validate_on_submit(self)
        if not rv:
            return False

        user = User.query.filter_by(email=self.email.data).first()
        if user is None:
            # Append the error to the password field no matter what so as not to give hints about what's wrong.
            self.password.errors.append(u'Invalid email or password.')
            return False

        if not user.check_password(self.password.data):
            self.password.errors.append(u'Invalid email or password.')
            return False

        self.user = user
        return True


class SignupForm(Form):
    email = StringField(u'Email', validators=[DataRequired(), Email()])
    password = PasswordField(u'Password', validators=[DataRequired()])
    password2 = PasswordField(u'Again', validators=[
        DataRequired(),
        EqualTo('password', message=u'Passwords must match')
    ])
    role = RadioField(u'Type', validators=[Required()],
                      choices=[(c, c) for c in USER_ROLES],
                      default=USER_ROLES[0])

    def validate_on_submit(self):
        rv = Form.validate_on_submit(self)
        if not rv:
            return False

        user = User.query.filter_by(email=self.email.data).first()
        if user is not None:
            self.email.errors.append(u'Email already exists')
            return False

        return True


class EditAccount(Form):
    curr_password = PasswordField('Current Password', [DataRequired()])
    new_password = PasswordField('New Password', [
        DataRequired(),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat New Password')

    def validate_on_submit(self):
        rv = Form.validate_on_submit(self)
        if not rv:
            return False

        if not current_user.check_password(self.curr_password.data):
            self.curr_password.errors.append(u'Invalid current password')
            return False

        return True


class AddBook(Form):
    name = StringField(u'Book Name', validators=[DataRequired(), length(max=80)])
    authors = StringField(u'Authors', validators=[DataRequired(), length(max=80)])
    isbn = StringField(u'ISBN', validators=[length(max=80)])
    category = SelectField(u'Category', validators=[DataRequired()],
                           choices=[(c, c) for c in BOOK_CATEGORIES])
    description = StringField(u'Description', validators=[length(max=255)])
    img = FileField(u'Book Cover', validators=[
        FileRequired(),
        FileAllowed(['jpg', 'png', 'jpeg', 'bmp'], 'Images only!')
    ])
    pdf = FileField(u'PDF File', validators=[
        FileRequired(),
        FileAllowed(['pdf'], 'PDF only!')
    ])

class EditBook(Form):
    name = StringField(u'Book Name', validators=[DataRequired(), length(max=80), ])
    authors = StringField(u'Authors', validators=[DataRequired(), length(max=80)])
    isbn = StringField(u'ISBN', validators=[length(max=80)])
    category = SelectField(u'Category', validators=[DataRequired()],
                           choices=[(c, c) for c in BOOK_CATEGORIES])
    description = StringField(u'Description', validators=[length(max=255)])
