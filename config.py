import os

"""
Application configuration
"""

_basedir = os.path.abspath(os.path.dirname(__file__))

# Folder where saves users pdf and books cover
UPLOAD_FOLDER = 'files/'

BOOK_CATEGORIES = [u'Novel', u'Fantastic', u'Science', u'Poetry', u'Other']

# The default role should be in USER_ROLES[0]
# uses in _base.html to show or not the dashboard link
USER_ROLES = [u'User', u'Publisher']

SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite:///%s/app.db' % _basedir)
SQLALCHEMY_POOL_RECYCLE = 60
SQLALCHEMY_ECHO = True

# Change before production deployment
SECRET_KEY = os.environ.get('SECRET_KEY', 'my precious')
DEBUG = True
